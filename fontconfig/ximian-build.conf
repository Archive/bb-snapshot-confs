<?xml version="1.0" ?>

<!DOCTYPE module SYSTEM "helix-build.dtd">

<!-- $Id$ -->

<module>
    <decl>
	<list id="devel_files">
	    <i>[[usrprefix]]/lib/lib*.a</i>
	    <i>[[usrprefix]]/lib/lib*.[[so]]</i>
	    <i>[[usrprefix]]/lib/pkgconfig/*.pc</i>
	    <i>[[usrprefix]]/include/fontconfig</i>
<!--	    <i>[[usrprefix]]/share/doc/fontconfig/fontconfig-devel*</i> -->
<!--	    <i>[[usrmandir]]/man3/*</i> -->
	</list>
    </decl>

    <targetset>
	<filter>
	    <i>.*</i>
	</filter>

	<rcsid>$Id$</rcsid>
	<name>fontconfig</name>
	<version>2.2.93</version>
	<rev>0</rev>
	<serial>1</serial>

	<srcname>fontconfig</srcname>

	<psdata id="copyright">MIT</psdata>

	<source>
	    <i>CVS :pserver:anoncvs@pdx.freedesktop.org:/cvs/fontconfig fontconfig HEAD</i>
	</source>

	<build id="default">
	    <unstripped />
	    <name>fontconfig</name>
	    <prepare>[[usrconfigure]]</prepare>
	    <compile>${MAKE}</compile>
	    <install>${MAKE} install DESTDIR=${DESTDIR}</install>
	    <package id="default">
		<name>fontconfig</name>

		<files>
		    <i>[[usrprefix]]/lib/lib*.[[so]].*</i>
		    <i>[[usrprefix]]/bin/fc-list</i>
		    <i>[[usrprefix]]/bin/fc-cache</i>
		    <i>[[usrsysconfdir]]/fonts/fonts.dtd</i>
<!--		    <i>[[usrprefix]]/share/doc/fontconfig/fontconfig-user*</i> -->
<!--		    <i>[[usrmandir]]/man5/*</i> -->
		</files>
		<conffiles>
		    <i>[[usrsysconfdir]]/fonts/fonts.conf</i>
		    <i>[[usrsysconfdir]]/fonts/local.conf</i>
		</conffiles>
		<docs>
		    <i>AUTHORS</i>
		    <i>COPYING</i>
		    <i>README</i>
		</docs>

		<script id="postinst">
		    <i>[[ldconfig]]</i>
		    <i>[[usrprefix]]/bin/fc-cache -f</i>
		</script>
		<script id="postrm">
		    <i>[[ldconfig]]</i>
		</script>
		<description>
<h>Font configuration and customization library</h>
<p>Fontconfig is designed to locate fonts within the
system and select them according to requirements specified by 
applications.</p>
		</description>
	    </package>

	    <package id="devel">
		<name>fontconfig-devel</name>
		<files>
		    <l>devel_files</l>
		</files>
		
		<description>
<h>Font configuration and customization library</h>
<p>The fontconfig-devel package includes the header files,
and developer docs for the fontconfig package.</p>
<p>Install fontconfig-devel if you want to develop programs which 
will use fontconfig.</p>
		</description>
	    </package>
	</build>
    </targetset>

    <targetset>
	<filter>
	    <i>rpm</i>
	</filter>
	
	<psdata id="url">http://keithp.com/fonts/</psdata>
	
	<build id="default">
	    <builddep id="buildrequires">
		<i>freetype-devel &gt;= 2.1.2-2</i>
		<i>expat-devel</i>
	        <i>lynx</i>
	    </builddep>
	    <package id="default">
		<psdata id="group">System Environment/Libraries</psdata>
		<dep id="requires">
		    <i>freetype &gt;= 2.1.2-2</i>
		</dep>
		<dep id="conflicts">
		    <i>[[name]]-devel &lt; [[fullversion]]</i>
		    <i>[[name]]-devel &gt; [[fullversion]]</i>
		</dep>
	    </package>
	    <package id="devel">
		<psdata id="group">Development/Libraries</psdata>
		<dep id="requires">
		    <i>[[name]] = [[fullversion]]</i>
		    <i>freetype-devel &gt;= 2.1.2-2</i>
		    <i>expat-devel</i>
		</dep>
	    </package>
	</build>
    </targetset>

    <targetset>
        <filter><i>suse</i></filter>
        <rev>0</rev>
        <cvspatch><i>fontconfig-dist-suse.patch-4</i></cvspatch>
        <build id="default">
            <builddep id="buildrequires">
                <i>freetype2-devel</i>
                <i>expat</i>
		<i>perl-SGMLS</i>
	        <i>lynx</i>
            </builddep>
            <package id="default">
                <dep id="requires">
                    <i>freetype2</i>
                    <i>expat</i>
                </dep>
            </package>
            <package id="devel"> <!-- suse 90 ships .la files for this module -->
                <files>
                    <l>devel_files</l>
                    <i>[[usrprefix]]/lib/lib*.la</i>
                </files>
                <dep id="requires">
                    <i>[[name]] = [[fullversion]]</i>
                    <i>freetype2-devel</i>
                    <i>expat</i>
                </dep>
            </package>
        </build>
    </targetset>

</module>

<!--
Local Variables:
mode: xml
End:
-->
