#! /usr/bin/perl -w

open (LIST, "find dest/ -type f -print |");
open (CHATZILLA, "> mozilla-chat.package");
open (DEVEL, "> mozilla-devel.package");
open (INSPECTOR, "> mozilla-dom-inspector.package");
open (DEFAULT, "> mozilla.package");
open (JSDEBUGGER, "> mozilla-js-debugger.package");
open (MAIL, "> mozilla-mail.package");
open (NSPR, "> mozilla-nspr.package");
open (NSS, "> mozilla-nss.package");
open (NSPRDEVEL, "> mozilla-nspr-devel.package");
open (NSSDEVEL, "> mozilla-nss-devel.package");
open (PSM, "> mozilla-psm.package");
open (DEAT, "> mozilla-deat.package");
open (UNNEEDED, "> mozilla-unneeded.package");
while (<LIST>) {
    s?dest/usr?\[\[prefix\]\]?;
    s/1.4/\[\[shortver\]\]/;
    s?\[\[prefix\]\]/share/man?\[\[mandir\]\]?;

    if (m,^.*\.idl$, ||
#	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/res/throbber/, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/res/rdf/dom-test-.\.xul$, ||
#	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/defaults/profile/.*\.css$, ||
	m,^\[\[prefix\]\]/lib/pkgconfig, ||
	m,.desktop$, ||
#	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/res/rdf/[^./]*\.xul$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/res/rdf/dom-test-4.css$,) {
	print UNNEEDED;
	next;
    }

#    if (m,\.xpm$,) {
#	if (! (m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/icons/mozicon16.xpm$, ||
#	       m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/icons/mozicon50.xpm$,)) {
#	    print UNNEEDED;
#	    next;
#	}
#    }

    if (m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/chrome/(lang/lang-)?de-AT.jar(.txt)?$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/chrome/(lang/lang-)?de-unix.jar(.txt)?$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/chrome/(lang/lang-)?AT.jar(.txt)?$,) {
	print DEAT;
	next;
    }

    if (m/chatzilla.*\.(jar|js)/) {
	print CHATZILLA;
	next;
    }

    if (m,^\[\[prefix\]\]/include/.*/nss/.*\.[^/]*$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/libnssckbi.a$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/libnss.a$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/libsmime.a$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/libsoftokn.a$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/libssl.a$,) {
	print NSSDEVEL;
	next;
    }

    if (m,^\[\[prefix\]\]/include/.*/nspr/.*\.[^/]*$, ||
        m,^\[\[prefix\]\]/share/aclocal/nspr.m4$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/libnspr4.a$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/libplc4.a$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/libplds4.a$, ||
	m,^\[\[prefix\]\]/bin/mozilla-config,) {
	print NSPRDEVEL;
	next;
    }

    if (m,^\[\[prefix\]\]/include/.*\.[^/]*$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/[^./]+.a$,) {
	print DEVEL;
	next;
    }

    if (m/inspector/g) {
	print INSPECTOR;
	if (m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/defaults/pref/inspector.js$,) {
	    print DEFAULT;
	}
	next;
    }

    if (m/venkman/g) {
	print JSDEBUGGER;
	next;
    }

    if (m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/addrbook.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libaddrbook.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/liblocalmail.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libmailnews.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libmime.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libmimeemitter.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libmsgdb.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libmsgimap.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libmsgnews.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libvcard.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libimport.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libimpText.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libimpComm4xMail.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libabsyncsvc.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/mailnews.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/mime.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/msgbase.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/msgcompose.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/msgdb.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/msgimap.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/msglocal.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/msgnews.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/msgsearch.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/nsLDAPPrefsService.js$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/txmgr.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/import.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/impComm4xMail.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/absync.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/chrome/messenger.jar$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libmsgsmime.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/msgsmime.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/smime-service.js$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libmsgmdn.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/mdn-service.js$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libmailview.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libbayesflt.so$,
	) {
	print MAIL;
	next;
    }

    if (m,^\[\[prefix\]\]/lib/libnspr4.so$, ||
	m,^\[\[prefix\]\]/lib/libplc4.so$, ||
	m,^\[\[prefix\]\]/lib/libplds4.so$,) {
	print NSPR;
	next;
    }

    if (m,^\[\[prefix\]\]/lib/libnssckbi.so$, ||
	m,^\[\[prefix\]\]/lib/libnss3.so$, ||
	m,^\[\[prefix\]\]/lib/libsmime3.so$, ||
	m,^\[\[prefix\]\]/lib/libsoftokn3.so$, ||
	m,^\[\[prefix\]\]/lib/libssl3.so$,) {
	print NSS;
	next;
    }
    if (m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/libnssckbi.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libpipboot.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/pipboot.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libpipnss.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/pipnss.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/libpippki.so$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/components/pippki.xpt$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/chrome/pipnss.jar$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/chrome/pippki.jar$, ||
	m,^\[\[prefix\]\]/lib/mozilla-\[\[shortver\]\]/libnssckbi.so$,) {
	print PSM;
	next;
    }

    if (m,^dest, ||
	m,\.debug$,) {
	print UNNEEDED;
	next;
    }

    print DEFAULT;
}
