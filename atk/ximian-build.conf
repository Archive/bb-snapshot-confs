<?xml version="1.0" ?>

<!DOCTYPE module SYSTEM "helix-build.dtd">

<module>
    <decl>

	<list id="devel_files">
	    <i>[[prefix]]/include/*</i>
	    <i>[[prefix]]/lib/lib*.[[so]]</i>
	    <i>[[prefix]]/lib/lib*.a</i>
	    <i>[[prefix]]/lib/pkgconfig/*</i>
	</list>

	<list id="doc_files">
	    <i>[[prefix]]/share/gtk-doc/html/*</i>
	</list>

    </decl>    
    <!-- Defaults for this package -->
    <packsys id="default">
	<os id="default">
	    <osvers id="default">
		<arch id="default">
		    <rcsid>$Id$</rcsid>
		    <name>atk</name>
		    <version>1.6.0</version>
		    <rev>0</rev>
		    <serial>1</serial>

		    <source><i>CVS :pserver:{{GNOMECVS_USER}}@cvs.gnome.org:/cvs/gnome [[name]] HEAD</i></source>

		    <build id="default">
		        <unstripped />
			<prepare>[[configure]] --enable-static --enable-gtk-doc</prepare>
			<compile>${MAKE}</compile>
			<install>${MAKE} install DESTDIR=${DESTDIR}</install>

			<package id="default">
			    <name>atk</name>
			    <files>
				<i>[[prefix]]/lib/lib*.[[so]].*</i>
				<i>[[prefix]]/share/locale/*/LC_MESSAGES/*</i>
			    </files>
			    <docs>
			        <i>AUTHORS</i>
				<i>COPYING</i>
				<i>ChangeLog</i>
				<i>INSTALL</i>
				<i>NEWS</i>
				<i>README</i>
			    </docs>
			    <script id="postinst">
				<i>[[ldconfig]]</i>
			    </script>
			    <script id="postrm">
				<i>[[ldconfig]]</i>
			    </script>
			    <description>
<h>Interfaces for Accessibility Support</h>
<p>The ATK library provides a set of interfaces for adding accessibility
support to applications and graphical user interface toolkits. By
supporting the ATK interfaces, an application or toolkit can be used
with tools such as screen readers, magnifiers, and alternative input
devices.</p>
			    </description>
			</package>
			<package id="devel">
			    <name>atk-devel</name>
			    <description>
<h>Interfaces for Accessibility Support</h>
<p>The atk-devel package includes the libraries, header files, and
developer docs for the atk package.</p>
<p>Install atk-devel if you want to develop programs which will use ATK.</p>
			    </description>
			    <files>
				<l>devel_files</l>
				<l>doc_files</l>
			    </files>
			</package>				
		    </build>
		</arch>
	    </osvers>
	</os>
    </packsys>

    <packsys id="rpm">
        <os id="default">
	    <osvers id="default">
	        <arch id="default">
		    <psdata id="url">http://developer.gnome.org/projects/gap/</psdata>
		    <psdata id="copyright">LGPL</psdata>
		    <build id="default">
			<builddep id="buildrequires">
			    <i>pkgconfig</i>
			    <i>glib2-devel &gt;= 2.0.0</i>
			    <i>gettext</i>
			    <i>perl</i>
			    <i>gawk</i>
			</builddep>
		        <package id="default">
			    <psdata id="group">System Environment/Libraries</psdata>
			    <dep id="conflicts">
				<i>[[name]]-devel &lt; [[fullversion]]</i>
				<i>[[name]]-devel &gt; [[fullversion]]</i>
			    </dep>
			</package>
			<package id="devel">
			    <psdata id="group">Development/Libraries</psdata>
			    <dep id="requires">
			        <i>[[name]] = [[fullversion]]</i>
				<i>glib2-devel</i>
			    </dep>
			</package>
		    </build>
		</arch>
	    </osvers>
	</os>
	<os id="mandrake">
	    <osvers id="default">
		<arch id="default">
		    <build id="default">
			<package id="default">
			    <name>libatk1.0_0</name>
			    <psdata id="group">System/Libraries</psdata>
			    <script id="postinst">
				<i>[[ldconfig]]</i>
			    </script>
			    <script id="postrm">
				<i>[[ldconfig]]</i>
			    </script>
			</package>
			<package id="devel">
			    <name>libatk1.0_0-devel</name>
			    <dep id="provides">
				<i>libatk-devel = %{PACKAGE_VERSION}</i>
				<i>libatk1.0-devel = %{PACKAGE_VERSION}</i>
				<i>atk-devel = %{PACKAGE_VERSION}</i>
			    </dep>
			    <dep id="requires">
			        <i>libatk1.0_0 = %{PACKAGE_VERSION}</i>
				<i>libglib2.0-devel</i>
			    </dep>
			</package>
		    </build>
		</arch>
	    </osvers>
	</os>
    </packsys>

    <packsys id="dpkg">
        <os id="default">
            <osvers id="default">
                <arch id="default">
                    <srcname>atk</srcname>
                    <psdata id="section">libs</psdata>
                    <psdata id="priority">extra</psdata>

                    <build id="default">
			<install>${MAKE} install DESTDIR=${DESTDIR}; rm -rf ${DESTDIR}/usr/lib/*.la; rm -rf ${DESTDIR}/usr/share/doc/libatk1.0-doc; mv debian/tmp/usr/share/gtk-doc/html/atk ${DESTDIR}/usr/share/doc/libatk1.0-doc; mkdir -p ${DESTDIR}/usr/lib/debug; cp -vdf ${DESTDIR}/usr/lib/*.so* ${DESTDIR}/usr/lib/debug</install>
			<builddep id="build-depends">
			    <i>debhelper (&gt;&gt; 3.0.0)</i>
			    <i>pkg-config</i>
			    <i>libglib2.0-dev (&gt;= 2.0.1)</i>
			</builddep>
                        <package id="default">
                            <name>libatk1.0-0</name>
                            <psdata id="section">libs</psdata>
                            <psdata id="architecture">any</psdata>
			    <changelog>ChangeLog</changelog>
                            <dep id="depends">
                                <i>${shlibs:Depends}</i>
                            </dep>
			    <docs>
			        <i>AUTHORS</i>
				<i>NEWS</i>
				<i>README</i>
			    </docs>
                            <files/>
                            <description>
<h>The ATK accessibility library</h>
<p>The ATK library provides a set of interfaces for accessibility.
By supporting the ATK interfaces, an application or toolkit can be used
such as tools such as screen readers, magnifiers, and alternative input
devices.</p>
<p>This package contains the shared libraries.</p>
                            </description>
                        </package>
                        <package id="libatk1.0-dbg">
                            <name>libatk1.0-dbg</name>
                            <psdata id="section">devel</psdata>
                            <psdata id="architecture">any</psdata>
                            <dep id="depends">
                                <i>libatk1.0-0 (= ${Source-Version})</i>
                            </dep>
                            <files>
                                <i>usr/lib/debug/lib*.so*</i>
                            </files>
                            <description>
<h>The ATK library and debugging symbols</h>
<p>The ATK library provides a set of interfaces for accessibility.
By supporting the ATK interfaces, an application or toolkit can be used
such as tools such as screen readers, magnifiers, and alternative input
devices.</p>
<p>This package contains unstripped shared libraries. it is provided
primarily to provide a backtrace with names in a debugger, this makes
it somewhat easier to interpret core dumps. The libraries are installed
in /usr/lib/debug and can be used by placing that directory in
LD_LIBRARY_PATH.
Most people will not need this package.</p>
                            </description>
                        </package>
                        <package id="devel">
                            <name>libatk1.0-dev</name>
                            <psdata id="section">devel</psdata>
                            <psdata id="architecture">any</psdata>
                            <dep id="replaces">
                                <i>libatk0-dev</i>
                                <i>libatk-dev</i>
                            </dep>
                            <dep id="depends">
                                <i>libatk1.0-0 (= ${Source-Version})</i>
                                <i>pkg-config</i>
                                <i>libglib2.0-dev (&gt;= 2.0.0)</i>
                            </dep>
                            <dep id="conflicts">
                                <i>libatk0-dev</i>
                                <i>libatk-dev</i>
                            </dep>
                            <files>
                                <i>usr/include/*</i>
                                <i>usr/lib/lib*.{so,la,a}</i>
                                <i>usr/lib/pkgconfig/*.pc</i>
                            </files>
                            <description>
<h>Development files for the ATK accessibility library</h>
<p>The ATK library provides a set of interfaces for accessibility.
By supporting the ATK interfaces, an application or toolkit can be used
such as tools such as screen readers, magnifiers, and alternative input
devices.</p>
<p>This package is needed to compile programs against libatk1.0-0,
as only it includes the header files and static libraries (optionally)
needed for compiling.</p>
                            </description>
                        </package>
                        <package id="libatk1.0-doc">
                            <name>libatk1.0-doc</name>
                            <psdata id="section">doc</psdata>
                            <psdata id="architecture">all</psdata>
                            <dep id="replaces">
                                <i>libatk-doc</i>
                            </dep>
                            <dep id="depends">
                                <i>lynx | www-browser</i>
                            </dep>
                            <dep id="conflicts">
                                <i>libatk-doc</i>
                            </dep>
                            <description>
<h>Documentation files for the ATK library</h>
<p>The ATK library provides a set of interfaces for accessibility.
By supporting the ATK interfaces, an application or toolkit can be used
such as tools such as screen readers, magnifiers, and alternative input
devices.</p>
<p>This package contains the HTML documentation for the ATK library in
/usr/share/doc/libatk1.0-doc/ .</p>
                            </description>
                        </package>
                    </build>
                </arch>
            </osvers>
        </os>
    </packsys>

    <targetset>
	<filter><i>suse-9</i></filter>
	<build id="default">
	    <package id="devel">
		<files>
		    <l>devel_files</l>
		    <i>[[prefix]]/lib/*.la</i>
		</files>
	    </package>

		<package id="doc">
			<name>atk-doc</name>

			<files><l>doc_files</l></files>

			<description>
<h>API Documentation for ATK</h>
<p>This packages contains documents describing ATK's API</p>
			</description>

			<psdata id="group">System Environment/Libraries</psdata>
		</package>

	</build>
    </targetset>

</module>

<!--
Local Variables:
mode: xml
End:
-->
