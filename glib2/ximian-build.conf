<?xml version="1.0" ?>

<!DOCTYPE module SYSTEM "helix-build.dtd">

<!-- $Id$ -->

<module>
    <decl>

	<list id="devel_files">
	    <i>[[prefix]]/lib/lib*.[[so]]</i>
	    <i>[[prefix]]/lib/lib*.a</i>
	    <i>[[prefix]]/lib/glib-2.0</i>
	    <i>[[mandir]]/man?/*</i>
	    <i>[[prefix]]/share/aclocal/*</i>
	    <i>[[prefix]]/bin/*</i>
	    <i>[[prefix]]/lib/pkgconfig/*.pc</i>
	    <i>[[prefix]]/share/glib-2.0</i>
	    <i>[[prefix]]/include/*</i>
	</list>

	<list id="doc_files">
		<i>[[prefix]]/share/gtk-doc</i>
	</list>

    </decl>    
    <!-- Defaults for this package -->
    <packsys id="default">
	<os id="default">
	    <osvers id="default">
		<arch id="default">
		    <name>glib2</name>

		    <rcsid>$Id$</rcsid>
		    <version>2.4.0</version>
		    <rev>0</rev>
		    <serial>1</serial>

		    <srcname>glib</srcname>
		    <psdata id="copyright">LGPL</psdata>
		    <source><i>CVS :pserver:{{GNOMECVS_USER}}@cvs.gnome.org:/cvs/gnome glib HEAD</i></source>
		    <patch/>

		    <build id="default">
		        <unstripped />
			<prepare>[[configure]] --enable-gtk-doc --enable-static; ${MAKE}</prepare>

			<compile>${MAKE}</compile>
			<install>[[install]]</install>
			<package id="default">
			    <name>glib2</name>
			    <files>
				<i>[[prefix]]/lib/lib*.[[so]].*</i>
				<i>[[prefix]]/*/locale/*/LC_MESSAGES/*.mo</i>
			    </files>
                            <script id="postinst">
				<i>[[ldconfig]]</i>
			    </script>
			    <script id="postrm">
				<i>[[ldconfig]]</i>
			    </script>
			    <description>
<h>A library of handy utility functions</h>
<p>This library is a collection of handy utility functions. It is
designed to solve some portability problems and provide other useful
functionality which most programs require.</p>
<p>GLib2 is used by GDK, GTK2 and many applications.  You should install
the glib2 package because many of your applications will depend on this
library.</p>
			    </description>
			    <docs>
				<i>AUTHORS</i>
				<i>COPYING</i>
				<i>ChangeLog</i>
				<i>NEWS</i>
				<i>README</i>
			    </docs>
			</package>

			<package id="devel">
			    <name>glib2-devel</name>
			    <files>
				<l>devel_files</l>
				<l>doc_files</l>
			    </files>
			    
			    <description>
<h>Development files for the GIMP Toolkit (Gtk2)</h>
<p>This package contains the header files and static libraries for the
GIMP Toolkit.</p>
<p>Install this package if you wish to develop your own X programs using
the GTK2 widget toolkit.</p>
			    </description>
			</package>
		    </build>

		    <build id="debug">
			<prepare>[[configure]] --enable-debug=yes</prepare>
			<compile>${MAKE}</compile>
			<install>[[install]]</install>
			<package id="default">
			    <name>glib2-debug</name>
			    <files>
				<i>[[prefix]]/lib/lib*.so.*</i>
			    </files>
			    <script id="postinst"><i>[[ldconfig]]</i></script>
			    <description>
<h>Debugging libraries for the GLib2 library</h>
<p>This package contains the debugging static libraries for the GLib2 C
library.</p>
<p>The debugging libraries are installed as //lib/libglib_g.a -- link
specifically with them if you want to debug.</p>
			    </description>
			</package>
		    </build>
		</arch>
	    </osvers>
	</os>
    </packsys>

    <packsys id="rpm">
	<os id="default">
	    <osvers id="default">
		<arch id="default">
		    <psdata id="url">http://www.gtk.org/</psdata>

		    <build id="default">
			<builddep id="buildrequires">
			    <i>pkgconfig &gt;= 0.14</i>
			    <i>gettext</i>
			</builddep>
			<package id="default">
			    <psdata id="group">System Environment/Libraries</psdata>
			    <dep id="conflicts">
				<i>[[name]]-devel &lt; [[fullversion]]</i>
				<i>[[name]]-devel &gt; [[fullversion]]</i>
			    </dep>
			</package>
			<package id="devel">
			    <psdata id="group">Development/Libraries</psdata>
			    <dep id="requires">
				<i>[[name]] = [[fullversion]]</i>
			    </dep>
			</package>
		    </build>
		</arch>
	    </osvers>
	</os>

	<os id="mandrake">
	    <osvers id="default">
		<arch id="default">
		    <build id="default">
			<package id="default">
			    <name>libglib2.0_0</name>
			    <dep id="provides">
				<i>libglib2 = %{PACKAGE_VERSION}</i>
				<i>libglib2.0 = %{PACKAGE_VERSION}</i>
				<i>glib2 = %{PACKAGE_VERSION}</i>
			    </dep>
			</package>
			<package id="devel">
			    <name>libglib2.0_0-devel</name>
			    <dep id="provides">
				<i>libglib2-devel = %{PACKAGE_VERSION}</i>
				<i>libglib2.0-devel = %{PACKAGE_VERSION}</i>
				<i>glib2-devel = %{PACKAGE_VERSION}</i>
			    </dep>
			    <dep id="requires">
				<i>libglib2.0_0 = %{PACKAGE_VERSION}</i>
			    </dep>
			</package>
		    </build>
		</arch>
	    </osvers>
            <osvers id="90">
		<arch id="default">
                    <build id="default">
                        <package id="devel">
			    <files>
				<i>[[prefix]]/lib/lib*.[[so]]</i>
				<i>[[prefix]]/lib/lib*.a</i>
				<i>[[prefix]]/lib/glib-2.0</i>
				<i>[[mandir]]/man?/*</i>
				<i>[[prefix]]/share/aclocal/glib-2.0.m4</i>
				<i>[[prefix]]/bin/glib-genmarshal</i>
				<i>[[prefix]]/bin/glib-mkenums</i>
				<i>[[prefix]]/bin/gobject-query</i>
				<i>[[prefix]]/lib/pkgconfig/*.pc</i>
				<i>[[prefix]]/share/gtk-doc/html/*</i>
				<i>[[prefix]]/include/*</i>
			    </files>
                        </package>
                        <package id="gettextize">
			    <name>glib-gettextize</name>
			    <psdata id="group">Development/Other</psdata>
			    <description>
<h>Gettextize replacement</h>
<p>glib2.0 package is designed to replace gettextize completely.
Various gettext related files are modified in glib and gtk+ to
allow better and more flexible i18n; however gettextize overwrites
them with its own copy of files, thus nullifying the changes.</p>
<p>If this replacement of gettextize is run instead, then all gnome
packages can potentially benefict from the changes.</p>
			    </description>
			    <files>
				<i>[[prefix]]/share/aclocal/glib-gettext.m4</i>
				<i>[[prefix]]/bin/glib-gettextize</i>
				<i>[[prefix]]/share/glib-2.0</i>
			    </files>
                        </package>
                    </build>
		</arch>
	    </osvers>
	</os>
    </packsys>

    <packsys id="dpkg">
        <os id="default">
            <osvers id="default">
                <arch id="default">
		    <name>glib2.0</name>

		    <tardir>glib-2.0.6</tardir>
		    <srcname>glib2.0</srcname>
                    <psdata id="section">libs</psdata>
                    <psdata id="priority">optional</psdata>
                    <build id="default">
			<builddep id="build-depends">
			    <i>debhelper (>> 2.0.0)</i>
			    <i>pkg-config</i>
			    <i>gettext</i>
			</builddep>

                        <package id="default">
                            <name>libglib2.0-0</name>
                            <psdata id="section">libs</psdata>
                            <psdata id="architecture">any</psdata>
			    <changelog>ChangeLog</changelog>
                            <dep id="depends">
                                <i>${shlibs:Depends}</i>
                            </dep>
                            <dep id="recommends">
                                <i>libglib2.0-data</i>
                            </dep>
			    <docs>
				<i>AUTHORS</i>
				<i>NEWS</i>
				<i>README</i>
			    </docs>
                            <description>
<h>The GLib library of C routines</h>
<p>GLib is a library containing many useful C routines for things such
as trees, hashes, and lists. GLib was previously distributed with
the GTK+ toolkit, but has been split off as of the developers' version
1.1.0.</p>
<p>You do not need to install this package if you have libgtk1 (note 1,
not 1.1 or 1.2) installed. libgtk1 included libglib in it. libgtk1.1
and libgtk1.2, however, do need libglib1.1 to be installed separately.</p>
			    </description>
			</package>

                        <package id="devel">
                            <name>libglib2.0-dev</name>
                            <psdata id="section">devel</psdata>
                            <psdata id="architecture">any</psdata>
                            <dep id="replaces">
                                <i>libglib1.3-dev</i>
                            </dep>
                            <dep id="depends">
                                <i>libglib2.0-0 (= ${Source-Version})</i>
				<i>libc6-dev</i>
				<i>pkg-config</i>
                            </dep>
                            <dep id="conflicts">
                                <i>libglib1.3-dev</i>
                            </dep>
                            <dep id="suggests">
                                <i>libgtk2.0-doc</i>
                            </dep>
                            <description>
				<h>Development files for GLib library</h>
<p>This package contains the header files and static libraries for the
GLib C library</p>
<p>Install this package if you wish to develop your own X programs using
the GTK+ widget toolkit.</p>
			    </description>
			</package>
			<package id="glib-doc">
			    <name>libglib2.0-doc</name>
			    <psdata id="section">misc</psdata>
			    <psdata id="architecture">all</psdata>
			    <dep id="depends">
				<i>lynx | www-browser</i>
			    </dep>
			    <dep id="replaces">
				<i>libglib1.3-doc</i>
			    </dep>
			    <dep id="conflicts">
				<i>libglib1.3-doc</i>
			    </dep>
			    <files>
				<i>/share/gtk-doc/*</i>
			    </files>
			    <description>
<h>Common files for GLib Library</h>
<p>GLib is a library containing many useful C routines for things such
as trees, hashes, lists, and strings. It is a useful general-purpouse
C library used by projects such as GTK+ and GNOME.
.
This package contains the HTML documenation for the GLib library in
//share/doc/libglib2.0-doc/ .</p>
			    </description>
			</package>

			<package id="glib-data">
			    <name>libglib2.0-data</name>
			    <psdata id="section">misc</psdata>
			    <psdata id="architecture">all</psdata>
			    <dep id="depends">
				<i>libglib2.0-0 (= ${Source-Version})</i>
			    </dep>
			    <dep id="replaces">
				<i>libglib1.3</i>
				<i>libglib1.3-data</i>
			    </dep>
			    <dep id="conflicts">
				<i>libglib1.3-data</i>
			    </dep>
			    <files>
				<i>/share/locale/*/LC_MESSAGES/*.mo</i>
			    </files>
			    <description>
<h>Common files for GLib Library</h>
<p>GLib is a library containing many useful C routines for things such
as trees, hashes, lists, and strings. It is a useful general-purpouse
C library used by projects such as GTK+ and GNOME.
.
This package contains the files which the runtime libraries need.</p>
			    </description>
			</package>
		    </build>

		    <build id="debug">
			<package id="default">
			    <name>libglib2.0-dbg</name>
			    <psdata id="section">devel</psdata>
			    <psdata id="priority">extra</psdata>
			    <psdata id="architecture">any</psdata>
			    <dep id="depends">
				<i>libglib2.0-0 (= ${Source-Version})</i>
			    </dep>
			    <files>
				<i>[[prefix]]/lib/debug/</i>
			    </files>
			    <script id="postinst">
				<i>[[ldconfig]]</i>
			    </script>
			    <script id="postrm"/>
			    <description>
<h>Debugging libraries for the GLib library</h>
<p>This package contains the debugging static libraries for the GLib C
library.</p>
<p>The debugging libraries are installed as //lib/libglib_g.a -- link
specifically with them if you want to debug.</p>
			    </description>
			</package>
		    </build>
		</arch>
	    </osvers>
	</os>
    </packsys>

    <targetset>
	<filter>
	    <i>solaris.*</i>
	</filter>
	<build id="default">
	    <prepare>[[configure]] --enable-static --with-libiconv=gnu</prepare>
	</build>
    </targetset>

    <targetset>
	<filter><i>hpux</i></filter>
	    <serial/>

	    
	    <build id="default">
		<name>GLib</name>

		<prepare>[[autogen]] --enable-static --with-libiconv=gnu</prepare>

		<psdata id="machine_type">*</psdata>
		<psdata id="os_release">?.11.*</psdata>
		<psdata id="os_version">*</psdata>
		<psdata id="fileset_prefix">GLIB</psdata>

		<package id="default">
		    <name>GLIB-SHLIBS</name>

		    <script id="postinst"><i>
# This only needs to get done once.  GLib is the base of all GNOME
# dependencies, so it's a reasonable place to put it.

if grep -q /opt/gnome/bin /etc/PATH; then
   true
else
   mv /etc/PATH /etc/PATH.old
   awk '{print $1 ":/opt/gnome/bin" }' &lt; /etc/PATH.old    &gt;/etc/PATH
   if [ -f /etc/PATH ]; then
       rm -f /etc/PATH.old
   fi
fi

if grep -q /opt/gnome/lib /etc/SHLIB_PATH; then
   true
else
   mv /etc/SHLIB_PATH /etc/SHLIB_PATH.old
   awk '{print $1 ":/opt/gnome/lib" }' &lt; /etc/SHLIB_PATH.old    &gt;/etc/SHLIB_PATH
   if [ -f /etc/SHLIB_PATH ]; then
       rm -f /etc/SHLIB_PATH.old
   fi
fi

if grep -q /opt/gnome/man /etc/MANPATH; then
   true
else
   mv /etc/MANPATH /etc/MANPATH.old
   awk '{print $1 ":/opt/gnome/man" }' &lt; /etc/MANPATH.old &gt;/etc/MANPATH
   if [ -f /etc/MANPATH ]; then
       rm -f /etc/MANPATH.old
   fi
fi
</i></script>
<script id="postrm"><i>
# The only way GLib is getting removed is if all GNOME software is
# also gone, so go ahead and remove the /etc/PATH etc. entries.

mv /etc/PATH /etc/PATH.old
mv /etc/SHLIB_PATH /etc/SHLIB_PATH.old
mv /etc/MANPATH /etc/MANPATH.old

sed 's|:/opt/gnome/bin||g' &lt; /etc/PATH.old    &gt;/etc/PATH
sed 's|:/opt/gnome/lib||g' &lt; /etc/SHLIB_PATH.old    &gt;/etc/SHLIB_PATH
sed 's|:/opt/gnome/man||g' &lt; /etc/MANPATH.old &gt;/etc/MANPATH

if [ -f /etc/PATH ]; then
    rm -f /etc/PATH.old
fi

if [ -f /etc/SHLIB_PATH ]; then
    rm -f /etc/SHLIB_PATH.old
fi

if [ -f /etc/MANPATH ]; then
    rm -f /etc/MANPATH.old
fi
	    </i></script>
	    </package>

	    <package id="devel">
		<name>GLIB-PRG</name>
		<psdata id="machine_type">*:32</psdata>
		<dep id="corequisites"><i>GLib.GLIB-SHLIBS,r>=[[version]].[[rev]]</i></dep>
		<files>
			<i>[[prefix]]/lib/lib*.[[so]]</i>
			<i>[[prefix]]/lib/lib*.a</i>
			<i>[[prefix]]/lib/glib-2.0</i>
			<i>[[prefix]]/share/aclocal/*</i>
			<i>[[prefix]]/bin/*</i>
			<i>[[prefix]]/include/*</i>
		</files>
	    </package>

	    <package id="man">
		<name>GLIB-MAN</name>
		<description><h>Manual pages for the GLib package</h><p></p></description>
		<files>
		    <i>[[mandir]]/man?/*</i>
		</files>
	    </package>
	</build>
    </targetset>

    <targetset>
	<filter><i>suse-82-.*|sld-.*</i></filter>
	<decl>
		<macro id="prefix">[[usrprefix]]</macro>
	</decl>
    </targetset>

    <targetset>
	<filter><i>suse-82|sld</i></filter>
	<build id="default">
	    <package id="devel">
		<files>
		    <l>devel_files</l>
		    <i>[[prefix]]/lib/*.la</i>
		    <l>doc_files</l>
		</files>
	    </package>
	</build>
    </targetset>

<!-- split into -doc package -->
    <targetset>
	<filter><i>suse-9</i></filter>
	<build id="default">

	<package id="devel">
	    <files>
		<l>devel_files</l>
		<i>[[prefix]]/lib/*.la</i>
	    </files>
	</package>

	<package id="doc">
		<name>glib2-doc</name>

		<files><l>doc_files</l></files>

		<description>
<h>A library of handy utility functions</h>
<p>This library is a collection of handy utility functions. It is
designed to solve some portability problems and provide other useful
functionality which most programs require.</p>
<p>GLib2 is used by GDK, GTK2 and many applications.  You should install
the glib2 package because many of your applications will depend on this
library.</p>
			    
		</description>

		<psdata id="group">System Environment/Libraries</psdata>
	</package>
	</build>
    </targetset>

</module>

<!--
Local Variables:
mode: xml
End:
-->
